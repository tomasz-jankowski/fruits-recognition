<div align="center">

  <h1>Fruits recognition</h1>

  <a href="https://www.python.org/"><img alt="Python" src="https://img.shields.io/badge/Python-3.7-%23404d59.svg?logo=Python&logoColor=white" height="20"/></a>
  <a href="https://www.tensorflow.org/"><img alt="TensorFlow" src="https://img.shields.io/badge/TensorFlow-2.1.0-%23404d59.svg?logo=TensorFlow&logoColor=white" height="20"/></a>

  <p><b>Deep learning project to recognize fruits and their quality.</b></p>

  </br>

  <sub>University project which allows to train convolutional neural network model and show its prediction on given images.<sub>

</div>


![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Installation

Install Python 3.7 and following libraries:
* numpy==1.18.1
* matplotlib==3.1.3
* opencv==3.4.2
* tensorflow==2.1.0
* tqdm==4.46.0
* tabulate==0.8.3

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Usage

There are three scripts which you may use:
* `training.py` to train a new model on images from `database` directory;
* `testing.py` to verify neural network model effectiveness on the test database;
* `classify.py` to check neural network model predictions on given image.

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Screenshot

![classify](https://user-images.githubusercontent.com/48838669/84918313-bb408a80-b0c0-11ea-921d-22909eec8dbc.png)

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Credit

[Michał Kliczkowski](https://github.com/michal090497) - co-developer

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## License

Licensed under [MIT](https://opensource.org/license/mit/).
